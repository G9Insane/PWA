/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.2.0/workbox-sw.js");

//debug mode

workbox.setConfig({ debug: true });
const precacheCacheName = workbox.core.cacheNames.precache;
const runtimeCacheName = workbox.core.cacheNames.runtime;
/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "about.html",
    "revision": "3e04fbd74fbd5b40209518b5a94ac48d"
  },
  {
    "url": "codes.html",
    "revision": "432ab5925cafe77adfd5387f69a8e4e1"
  },
  {
    "url": "css/bootstrap.css",
    "revision": "2183d05f5a0a9a3b2e8cb0509ca363e3"
  },
  {
    "url": "css/fasthover.css",
    "revision": "543cd21a80772ea80192f492e3305e03"
  },
  {
    "url": "css/flexslider.css",
    "revision": "ea057492ca824ddc6f561ff5e6d3d4c2"
  },
  {
    "url": "css/font-awesome.css",
    "revision": "a16730221cf9c8b1bad3dd5419edf16b"
  },
  {
    "url": "css/jquery.countdown.css",
    "revision": "c42533fe2a2ff7839a4163ad60ddfc84"
  },
  {
    "url": "css/popuo-box.css",
    "revision": "67052aae732f3398ba7ec0200bd6a8ad"
  },
  {
    "url": "css/style.css",
    "revision": "2dfc3f584ddb6bd8dea8c8513208c285"
  },
  {
    "url": "faq.html",
    "revision": "d4369d7a8f16765b661921197c6cd786"
  },
  {
    "url": "favicon.ico",
    "revision": "22298544979c4a6950cea9dc5423a3cd"
  },
  {
    "url": "fonts/fontawesome-webfont.eot",
    "revision": "8b27bc96115c2d24350f0d09e6a9433f"
  },
  {
    "url": "fonts/fontawesome-webfont.svg",
    "revision": "0a799148a50bb02c6f380eabd8d97559"
  },
  {
    "url": "fonts/fontawesome-webfont.ttf",
    "revision": "dcb26c7239d850266941e80370e207c1"
  },
  {
    "url": "fonts/fontawesome-webfont.woff",
    "revision": "3293616ec0c605c7c2db25829a0a509e"
  },
  {
    "url": "fonts/fontawesome-webfont.woff2",
    "revision": "e6cf7c6ec7c2d6f670ae9d762604cb0b"
  },
  {
    "url": "fonts/FontAwesome.otf",
    "revision": "3f3a623e88cb5c62eaa2367195e98b67"
  },
  {
    "url": "fonts/glyphicons-halflings-regular.woff",
    "revision": "fa2772327f55d8198301fdb8bcfc8158"
  },
  {
    "url": "fonts/glyphicons-halflings-regular.woff2",
    "revision": "448c34a56d699c29117adc64c43affeb"
  },
  {
    "url": "icons.html",
    "revision": "749fe64e0ecb251bb1c8ea58afbad2d4"
  },
  {
    "url": "images/1.jpg",
    "revision": "d499343a7e40128097d8541a8d4ca117"
  },
  {
    "url": "images/10.jpg",
    "revision": "2939ec521c90df54793eab6d05730b50"
  },
  {
    "url": "images/11.jpg",
    "revision": "adcaba17905303e94df53a8e45d96065"
  },
  {
    "url": "images/12.jpg",
    "revision": "ba65a51ce8511ca05f2dec398209a87d"
  },
  {
    "url": "images/13.jpg",
    "revision": "503b45e99764c062ceec44b10710f59c"
  },
  {
    "url": "images/14.jpg",
    "revision": "c76b341c3dca9a166692e608a39bb6f1"
  },
  {
    "url": "images/15.jpg",
    "revision": "d70db4055d10079a4ed7fbc21b6997c2"
  },
  {
    "url": "images/16.jpg",
    "revision": "ce986a6bf96c439f793a4e67f4a813a5"
  },
  {
    "url": "images/17.jpg",
    "revision": "764522632405439ac6e69ecf74e599f2"
  },
  {
    "url": "images/18.jpg",
    "revision": "2976d62140d43848a327fa076e1669a2"
  },
  {
    "url": "images/19.jpg",
    "revision": "ca60cdf65f1a78e991ac9ff74a2870c7"
  },
  {
    "url": "images/2.jpg",
    "revision": "d3edb3311191149f3d292d06d9b7602a"
  },
  {
    "url": "images/20.jpg",
    "revision": "7d6afeb803faa0b484830887e68e7334"
  },
  {
    "url": "images/21.jpg",
    "revision": "60ae2d682aa29afea855b1ae14d8de0f"
  },
  {
    "url": "images/22.jpg",
    "revision": "332102ac6dd80ede61ab4fbed4d39bbb"
  },
  {
    "url": "images/23.jpg",
    "revision": "c2899e17d424af1724ef385cf3190113"
  },
  {
    "url": "images/24.jpg",
    "revision": "64d2cbabde63a7395d9ae9e8194a9d8c"
  },
  {
    "url": "images/25.jpg",
    "revision": "ec601abf57d9f50ceeb875e3d90b4f11"
  },
  {
    "url": "images/26.jpg",
    "revision": "74e257f043f72d3fcf08fbc9884121c5"
  },
  {
    "url": "images/27.jpg",
    "revision": "c273d529315d5abe23e4fde4985a6eab"
  },
  {
    "url": "images/28.jpg",
    "revision": "dcc081b76d82a8c5b7db9883f77bdf65"
  },
  {
    "url": "images/29.jpg",
    "revision": "b34c0c3a5a60956917610c76d1461a34"
  },
  {
    "url": "images/3.jpg",
    "revision": "cbe8adfa0a4b338210421385b3713d85"
  },
  {
    "url": "images/30.jpg",
    "revision": "434a6119624e30d318eda2c8d8bde6c4"
  },
  {
    "url": "images/31.jpg",
    "revision": "c48f1f021a7d3d6cd69a64534afa99b9"
  },
  {
    "url": "images/32.jpg",
    "revision": "7eceeab26a7575483fd5beaf03fb9983"
  },
  {
    "url": "images/33.jpg",
    "revision": "1aa7bd172366eda84f48669e6a8fc207"
  },
  {
    "url": "images/34.jpg",
    "revision": "6f6f21643ad0d811cdde73fc9b2f6305"
  },
  {
    "url": "images/35.jpg",
    "revision": "4faf86565c9789fcf477b48d40e65b7d"
  },
  {
    "url": "images/36.jpg",
    "revision": "6e3c64308b690b071bc806dae5f7357f"
  },
  {
    "url": "images/37.jpg",
    "revision": "7394251860a436c5acad14ef55985ba3"
  },
  {
    "url": "images/38.jpg",
    "revision": "8348c80e007c8a7e34796a7eb00276bb"
  },
  {
    "url": "images/39.jpg",
    "revision": "f381159d1ae244703d51440d711b685a"
  },
  {
    "url": "images/4.jpg",
    "revision": "bc855312fcdb57f967018aea1e78d4db"
  },
  {
    "url": "images/40.jpg",
    "revision": "f4c6881f1326320e646ec64b078718f8"
  },
  {
    "url": "images/41.jpg",
    "revision": "35c508571e020bdeb7ba833b1a3040d8"
  },
  {
    "url": "images/44.jpg",
    "revision": "3015ae35adf516ff46aa9a028a5265ea"
  },
  {
    "url": "images/45.jpg",
    "revision": "2529579bfd96ce7c96b65c84050e7467"
  },
  {
    "url": "images/46.jpg",
    "revision": "89620c5cc366424627bad2b305fc8194"
  },
  {
    "url": "images/47.jpg",
    "revision": "8bb6b97af4e10e5b9c38b05dc9fa29f0"
  },
  {
    "url": "images/48.jpg",
    "revision": "1f5dc03ad303a25005276e8d56255c49"
  },
  {
    "url": "images/49.jpg",
    "revision": "41b991f3aab8234c609cb5d37109debd"
  },
  {
    "url": "images/5.jpg",
    "revision": "7eedf060f7bed02760d0b13092428d12"
  },
  {
    "url": "images/50.jpg",
    "revision": "61e2df262ad825387882ecca21f402f2"
  },
  {
    "url": "images/51.jpg",
    "revision": "931a40ad3cb76c92114817ae50949219"
  },
  {
    "url": "images/52.jpg",
    "revision": "54fc08bc7217f90bc355b5690a2bcea0"
  },
  {
    "url": "images/6.jpg",
    "revision": "4f37c2d0bb5127b081090709c44a5b85"
  },
  {
    "url": "images/7.jpg",
    "revision": "cb7d732894c0f6ebff786732487ac0e6"
  },
  {
    "url": "images/8.jpg",
    "revision": "7736628fef60db1b2806cf0af7b522bf"
  },
  {
    "url": "images/9.jpg",
    "revision": "d34837d19387218d249eb595016326b3"
  },
  {
    "url": "images/a.jpg",
    "revision": "92cb3b9bc32acba9889d4f895003ec1e"
  },
  {
    "url": "images/arrow.png",
    "revision": "b6ea48755791c676194e3d4fa0f1f167"
  },
  {
    "url": "images/b.jpg",
    "revision": "be382c939ef8101f983b48ef27c18d29"
  },
  {
    "url": "images/b1.jpg",
    "revision": "49900e245f8b7f125e5215f28d7fb697"
  },
  {
    "url": "images/bg1.jpg",
    "revision": "f0735983882829b9fb943f43990c19dd"
  },
  {
    "url": "images/bg2.jpg",
    "revision": "0d72b162f78f49cfc8876ad95b668b50"
  },
  {
    "url": "images/bg3.jpg",
    "revision": "7ebd2f9a1365ed45d948260bf768743a"
  },
  {
    "url": "images/c.jpg",
    "revision": "84331b2b75994348e419396b9566b879"
  },
  {
    "url": "images/check.png",
    "revision": "4f8f4206fae3f13c126cbbf438f2ffa1"
  },
  {
    "url": "images/close.png",
    "revision": "566fff5f8a733cdd391295bb8d1e551b"
  },
  {
    "url": "images/img-sp.png",
    "revision": "374206e819e9dfd212039314c189b584"
  },
  {
    "url": "images/left-arrow.png",
    "revision": "67060a3132adbfc12442737a958913a9"
  },
  {
    "url": "images/p1.jpg",
    "revision": "3d9ee99a3b2e822106f29ed5aa41ec81"
  },
  {
    "url": "images/p10.jpg",
    "revision": "c6e59a9033aae893ab6b91af312f15e2"
  },
  {
    "url": "images/p11.jpg",
    "revision": "886e963dbcb6e569c3556502db6465b4"
  },
  {
    "url": "images/p12.jpg",
    "revision": "e6b4fd9b3cb503108657a052ae6ab80c"
  },
  {
    "url": "images/p13.jpg",
    "revision": "18c1f58f11240e3096b719c6c2313ff6"
  },
  {
    "url": "images/p2.jpg",
    "revision": "dc8e03bb0e66ba5a374fd48f1490ed3f"
  },
  {
    "url": "images/p3.jpg",
    "revision": "3898276e6f220fd86154b0b9f4ab9eb3"
  },
  {
    "url": "images/p4.jpg",
    "revision": "50bf12216b840491466ab9f642c4470e"
  },
  {
    "url": "images/p5.jpg",
    "revision": "899bb46669bb824c09833b4ce4c63cfe"
  },
  {
    "url": "images/p6.jpg",
    "revision": "bb9a5ae946447e1fc3390c1ea53d5f14"
  },
  {
    "url": "images/P7.jpg",
    "revision": "a975072a7bdf918c913f952dcceaacb1"
  },
  {
    "url": "images/p8.jpg",
    "revision": "dbac245d0d8f562610fd473a30311573"
  },
  {
    "url": "images/p9.jpg",
    "revision": "9f1b36aff76e86c200de4c916d84bab2"
  },
  {
    "url": "images/right-arrow.png",
    "revision": "e4be7fb7b9f450c7cd41214a36feb7dc"
  },
  {
    "url": "images/star-.png",
    "revision": "3928e75e04afe9ae9fe75c39cfd35e5c"
  },
  {
    "url": "images/star.png",
    "revision": "0b9a12384c3d67769d624ac74af2e48f"
  },
  {
    "url": "images/t1.png",
    "revision": "3378adf024f8e6a75e82efe112961d81"
  },
  {
    "url": "images/t2.png",
    "revision": "3c4c461ffea4b42122f0d33b1334405c"
  },
  {
    "url": "images/t3.png",
    "revision": "48933dd64493845d0d8900d4d150a99f"
  },
  {
    "url": "images/t4.png",
    "revision": "f18102542550352d3c55ef1b9cbccfbb"
  },
  {
    "url": "images/t5.png",
    "revision": "a5eeabdcd1c6579d3a4b983d6a4fe8f7"
  },
  {
    "url": "images/t6.png",
    "revision": "c978bd42d3654c2478ff9a95aa3469dd"
  },
  {
    "url": "images/t7.png",
    "revision": "e3d38c09329f1a395baf284dd902178c"
  },
  {
    "url": "images/tb1.jpg",
    "revision": "a71e061ad161d6db167cb2355fcd3b9b"
  },
  {
    "url": "images/tb2.jpg",
    "revision": "e25fcec80778a528c24211131432f4f1"
  },
  {
    "url": "images/tb3.jpg",
    "revision": "26819e54cd1172cc46cfc73cf50b9e2f"
  },
  {
    "url": "images/tb4.jpg",
    "revision": "42859f333b77c55bf1243b8609416552"
  },
  {
    "url": "images/tb5.jpg",
    "revision": "88b813670fcb6cab8e5a5f81bc3bbe75"
  },
  {
    "url": "index.html",
    "revision": "d45687d45d756c9b0682b5d74bb4a6e4"
  },
  {
    "url": "index1.html",
    "revision": "37c5c0cef753e133500b8739da32bbc8"
  },
  {
    "url": "js/bootstrap-3.1.1.min.js",
    "revision": "ba847811448ef90d98d272aeccef2a95"
  },
  {
    "url": "js/easyResponsiveTabs.js",
    "revision": "bc9ed6776b0d682dcb6b6f5b3a52adfd"
  },
  {
    "url": "js/imagezoom.js",
    "revision": "c15e52c613c4f1f7fc2cf0f22638bd41"
  },
  {
    "url": "js/jquery.countdown.js",
    "revision": "91fea9d7f7d6510c6fc144f1f0ba50b2"
  },
  {
    "url": "js/jquery.flexisel.js",
    "revision": "52d7d749792c7af165d67b9de6d49147"
  },
  {
    "url": "js/jquery.flexslider.js",
    "revision": "ca62c1c70540c707878ca4ef68ce4f22"
  },
  {
    "url": "js/jquery.magnific-popup.js",
    "revision": "c8f9c10f7b896edaaa478913d146bd7e"
  },
  {
    "url": "js/jquery.min.js",
    "revision": "b8d64d0bc142b3f670cc0611b0aebcae"
  },
  {
    "url": "js/jquery.wmuSlider.js",
    "revision": "8023da3ed8f32eacd593222262cd953b"
  },
  {
    "url": "js/minicart.js",
    "revision": "ba7089853a4895906fd8644cfb4a4dc8"
  },
  {
    "url": "js/script.js",
    "revision": "faa70c420ac777e2ce82c5c76aa736a3"
  },
  {
    "url": "mail.html",
    "revision": "dfd7444bffb345d81b5600e185f3e7cd"
  },
  {
    "url": "manifest.json",
    "revision": "f470e42da18e5a10fe215074026b8133"
  },
  {
    "url": "products.html",
    "revision": "adfbfc56e29ba7138786887501d8f6e3"
  },
  {
    "url": "products1.html",
    "revision": "abeb05b34ef906ead5285f6df289bf7c"
  },
  {
    "url": "products2.html",
    "revision": "99b4343bcde3363516fc6909292307dd"
  },
  {
    "url": "single.html",
    "revision": "649666df1400329c5ef9c7a935694773"
  },
  {
    "url": "src/css/app.css",
    "revision": "61697f2c1196093074b2779e9b5a4ab3"
  },
  {
    "url": "src/images/icons/app-icon-144x144.png",
    "revision": "56ee7d5f844cdbeed921230cc2075491"
  },
  {
    "url": "src/images/icons/app-icon-256x256.png",
    "revision": "fc3d93ebdbfdac72c9b52853220d5454"
  },
  {
    "url": "src/images/icons/app-icon-512x512.png",
    "revision": "a00d3c90b6065a1e6b5ea853b22ad650"
  },
  {
    "url": "src/images/icons/app-icon-96x96.png",
    "revision": "ac96a1075611ebca9ba9ec8ef131b3d1"
  },
  {
    "url": "src/images/pwa.jpg",
    "revision": "e284e024a0638ca89138502648f3eb32"
  },
  {
    "url": "src/js/app.js",
    "revision": "845a0065a07764662a0ce0f29c93e53e"
  },
  {
    "url": "sw2.js",
    "revision": "03110b18ccb9609e1f8202824bce06a7"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

// The most verbose - displays all logs.
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);

// Shows logs, warnings and errors.
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.log);

// Show warnings and errors.
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.warn);

// Show *just* errors
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.error);

// Silence all of the Workbox logs.
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.silent);


// workbox.googleAnalytics.initialize();
// workbox.core.setCacheNameDetails({
//     prefix: 'my-app',
//     suffix: 'v1',
//     precache: 'custom-precache-name',
//     runtime: 'custom-runtime-name'
// });
//
//
// //Caching Images
// workbox.routing.registerRoute(
//     /.*\.(?:png|jpg|jpeg|svg|gif)/g,
//     workbox.strategies.CacheFirst({
//         cacheName: 'image-cache',
//     })
// );
//
//
//
// //font cache
// workbox.routing.registerRoute(
//     new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
//     workbox.strategies.cacheFirst({
//         cacheName: 'googleapis',
//         plugins: [
//             new workbox.expiration.Plugin({
//                 maxEntries: 30,
//             }),
//         ],
//     }),
// );
//
// //caching js css
// workbox.routing.registerRoute(
//     /\.(?:js|css)$/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'resources',
//     }),
// );


