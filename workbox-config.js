module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{html,css,ico,eot,svg,ttf,woff,woff2,otf,jpg,png,js,json}"
  ],
  "swDest": "public\\sw.js",
    // Define runtime caching rules.
    runtimeCaching: [
        {
            // Match any request ends with .png, .jpg, .jpeg or .svg.
            urlPattern: /\.(?:png|jpg|jpeg|svg)$/,

            // Apply a cache-first strategy.
            handler: 'cacheFirst',

        },
    ],
};